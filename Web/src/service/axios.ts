import axios from "axios";

const instance = axios.create({
  baseURL: "http://localhost:8000",
});
const http = instance;

function MatchMovie(movie_name: any) {
  return http.put("/reccomendations", movie_name);
}

export default { MatchMovie };
