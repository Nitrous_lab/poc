
## Python Lib

```sh
pip install joblib
```

```sh
pip install fastapi
```

```sh
pip install "uvicorn[standard]"
```
```sh
pip install numpy
```

## Run FastApi
```sh
uvicorn __init__:app --reload
```