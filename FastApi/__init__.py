from typing import Union
import joblib
from fastapi import FastAPI
from pydantic import BaseModel
import numpy as np
from fastapi.middleware.cors import CORSMiddleware
import pandas as pd
import csv
import json

data = []


# Function to convert a CSV to JSON
# Takes the file paths as arguments
def make_json(csvFilePath, jsonFilePath):
	
	# create a dictionary
	
	
	# Open a csv reader called DictReader
	with open(csvFilePath, encoding='utf-8') as csvf:
		csvReader = csv.DictReader(csvf)
		
		# Convert each row into a dictionary 
		# and add it to data
		for rows in csvReader:
			
			# Assuming a column named 'No' to
			# be the primary key
			key = rows['id']
			data[key] = rows['title']

	# Open a json writer, and use the json.dumps() 
	# function to dump data
	with open(jsonFilePath, 'w', encoding='utf-8') as jsonf:
		jsonf.write(json.dumps(data, indent=4))
		
# Driver Code

# Decide the two file paths according to your 
# computer system
csvFilePath = r'movie_title.csv'
jsonFilePath = r'movie_title.json'

# Call the make_json function
make_json(csvFilePath, jsonFilePath)

app = FastAPI()
origins = [
    "http://localhost.tiangolo.com",
    "https://localhost.tiangolo.com",
    "http://localhost",
    "http://localhost:8080",
    "http://localhost:5173",

]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)



cosine_sim = joblib.load('finalmodel.pkl')
df = pd.read_csv('movie_n_Big_Key_words.csv')
AllMovie = pd.read_csv('movie_title.csv')
AllMovie = AllMovie['title']


indices = pd.Series(df.index)




class Movie(BaseModel):
    title: str
    is_offer: Union[bool, None] = None

def recommendations(title, cosine_sim = cosine_sim):
    
    # initializing the empty list of recommended movies
    recommended_movies = []
    
    # gettin the index of the movie that matches the title
    idx = indices[indices == title].index[0]

    # creating a Series with the similarity scores in descending order
    score_series = pd.Series(cosine_sim[idx]).sort_values(ascending = False)

    # getting the indexes of the 10 most similar movies
    top_10_indexes = list(score_series.iloc[1:11].index)
    
    # populating the list with the titles of the best 10 matching movies
    for i in top_10_indexes:
        recommended_movies.append(list(df.index)[i])
        
    return recommended_movies

@app.get("/list")
def read_root():
    print(data)
    return data

@app.put("/recommendations")
async def update_item(item: Movie):
    
    # movie_List = recommendations(item.title)

    return { "List" : movie_List}
